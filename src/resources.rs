//! Managing resources (textures, mostly)
use image::RgbaImage;
use once_cell::sync::Lazy;

/// Statically loaded resources
pub static RESOURCES: Lazy<Resources> = Lazy::new(|| {
    log::info!("Loading resources..");
    Resources::load().expect("Some assets were not found! Check your ./assets dir")
});

pub struct Resources {
    pub white_board: RgbaImage,
    pub white_king: RgbaImage,
    pub white_rook: RgbaImage,
    pub white_queen: RgbaImage,
    pub white_knight: RgbaImage,
    pub white_bishop: RgbaImage,
    pub white_pawn: RgbaImage,
    pub black_board: RgbaImage,
    pub black_king: RgbaImage,
    pub black_rook: RgbaImage,
    pub black_queen: RgbaImage,
    pub black_knight: RgbaImage,
    pub black_bishop: RgbaImage,
    pub black_pawn: RgbaImage,
}

impl Resources {
    /// Load assets from the /assets directory
    pub fn load() -> image::error::ImageResult<Self> {
        let white_board = image::open("assets/white/board.png")?.into_rgba8();
        let white_king = image::open("assets/white/king.png")?.into_rgba8();
        let white_rook = image::open("assets/white/rook.png")?.into_rgba8();
        let white_queen = image::open("assets/white/queen.png")?.into_rgba8();
        let white_bishop = image::open("assets/white/bishop.png")?.into_rgba8();
        let white_knight = image::open("assets/white/knight.png")?.into_rgba8();
        let white_pawn = image::open("assets/white/pawn.png")?.into_rgba8();

        let black_board = image::open("assets/black/board.png")?.into_rgba8();
        let black_king = image::open("assets/black/king.png")?.into_rgba8();
        let black_rook = image::open("assets/black/rook.png")?.into_rgba8();
        let black_queen = image::open("assets/black/queen.png")?.into_rgba8();
        let black_bishop = image::open("assets/black/bishop.png")?.into_rgba8();
        let black_knight = image::open("assets/black/knight.png")?.into_rgba8();
        let black_pawn = image::open("assets/black/pawn.png")?.into_rgba8();

        Ok(Self {
            white_board,
            white_king,
            white_rook,
            white_queen,
            white_bishop,
            white_knight,
            white_pawn,
            black_board,
            black_king,
            black_rook,
            black_queen,
            black_bishop,
            black_knight,
            black_pawn,
        })
    }
}

/// Looks up piece in the resources and returns it
pub fn get_piece_from_resources<'a>(
    resources: &'a Resources,
    piece: &shakmaty::Piece,
) -> &'a RgbaImage {
    use shakmaty::Role::*;
    match piece.role {
        Pawn => {
            if piece.color.is_white() {
                &resources.white_pawn
            } else {
                &resources.black_pawn
            }
        }
        Knight => {
            if piece.color.is_white() {
                &resources.white_knight
            } else {
                &resources.black_knight
            }
        }
        Bishop => {
            if piece.color.is_white() {
                &resources.white_bishop
            } else {
                &resources.black_bishop
            }
        }
        Rook => {
            if piece.color.is_white() {
                &resources.white_rook
            } else {
                &resources.black_rook
            }
        }
        Queen => {
            if piece.color.is_white() {
                &resources.white_queen
            } else {
                &resources.black_queen
            }
        }
        King => {
            if piece.color.is_white() {
                &resources.white_king
            } else {
                &resources.black_king
            }
        }
    }
}
