//! Other internal functions
use shakmaty::san::{ParseSanError, San};

pub fn text2san(input: &str) -> Result<San, ParseSanError> {
    input.trim().parse()
}
