//! Rendering the chess board
use image::Pixel;
use shakmaty::{Chess, Position, Square};

use crate::resources::{get_piece_from_resources, RESOURCES};

/// The main fuction to render a chess game
pub fn render_to_img(game: &Chess) {
    let mut render = match game.turn() {
        shakmaty::Color::White => RESOURCES.white_board.clone(),
        shakmaty::Color::Black => RESOURCES.black_board.clone(),
    };
    let mut board = game.board().clone();
    board.flip_vertical();
    if game.turn().is_black() {
        board.rotate_180()
    }
    for (piece, pos) in board.into_iter().map(|(square, piece)| {
        let mut pos = square_to_rel_pos(&square);
        pos.x -= 1;
        pos.y -= 1;
        pos.x *= 60;
        pos.y *= 60;
        (piece, pos)
    }) {
        for x in 0..60 {
            for y in 0..60 {
                let board_pixel = render.get_pixel_mut(x + pos.x, y + pos.y);
                let piece_pixel = get_piece_from_resources(&RESOURCES, &piece).get_pixel(x, y);
                board_pixel.blend(piece_pixel);
            }
        }
    }
    // If this fails, something has gone terribly wrong
    render.save("renders/latest.png").unwrap();
}

#[derive(Debug)]
pub struct Pos {
    x: u32,
    y: u32,
}

/// Convert square on the board to relative position
/// # Examples:
/// The square a1 becomes x: 1, y: 1
///
/// The sqare e4 becomes x: 5, y: 4
pub fn square_to_rel_pos(square: &Square) -> Pos {
    use shakmaty::File::*;
    use shakmaty::Rank::*;
    let x = match square.file() {
        A => 1,
        B => 2,
        C => 3,
        D => 4,
        E => 5,
        F => 6,
        G => 7,
        H => 8,
    };
    let y = match square.rank() {
        First => 1,
        Second => 2,
        Third => 3,
        Fourth => 4,
        Fifth => 5,
        Sixth => 6,
        Seventh => 7,
        Eighth => 8,
    };
    Pos { x, y }
}
