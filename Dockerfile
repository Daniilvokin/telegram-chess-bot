FROM rust:latest as builder

WORKDIR /usr/src/telegram-chess-bot
COPY . .
ENV CARGO_REGISTRIES_CRATES_IO_PROTOCOL=sparse
RUN cargo install --path .

FROM debian:bullseye-slim
COPY --from=builder /usr/local/cargo/bin/telegram-chess-bot /usr/local/bin/telegram-chess-bot
CMD ["telegram-chess-bot"]