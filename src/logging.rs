//! Simple  log4js configuration
use anyhow::Result;
use console::style;
use log4rs::append::console::ConsoleAppender;
use log4rs::append::file::FileAppender;
use log4rs::config::Logger;
use log4rs::config::{Appender, Root};
use log4rs::encode::pattern::PatternEncoder;

pub fn setup_logging() -> Result<()> {
    let logfile = FileAppender::builder()
        .encoder(Box::new(PatternEncoder::new("{d} - {m}{n}")))
        .build("log/output.log")?;
    let d = style("{d(%d-%m-%Y %H:%M:%S)}").cyan().to_string();
    let m = style("{m}").dim().to_string();
    let l = style("{l}").bold().to_string();
    let module = style("{M}").yellow().to_string();
    let pattern: String = format!("[{d}] [{module}] {l} - {m}\n");
    let stdout = ConsoleAppender::builder()
        .encoder(Box::new(PatternEncoder::new(pattern.as_str())))
        .build();

    if cfg!(debug_assertions) {
        let config = log4rs::config::Config::builder()
            .appender(Appender::builder().build("stdout", Box::new(stdout)))
            .appender(Appender::builder().build("logfile", Box::new(logfile)))
            .logger(Logger::builder().build("telegram_chess_bot", log::LevelFilter::Debug))
            .build(
                Root::builder()
                    .appender("stdout")
                    .appender("logfile")
                    .build(log::LevelFilter::Warn),
            )?;
        log4rs::init_config(config)?;
    } else {
        let config = log4rs::config::Config::builder()
            .appender(Appender::builder().build("stdout", Box::new(stdout)))
            .appender(Appender::builder().build("logfile", Box::new(logfile)))
            .logger(Logger::builder().build("telegram_chess_bot", log::LevelFilter::Info))
            .build(
                Root::builder()
                    .appender("stdout")
                    .appender("logfile")
                    .build(log::LevelFilter::Info),
            )?;
        log4rs::init_config(config)?;
    }

    Ok(())
}
