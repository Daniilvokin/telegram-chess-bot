//! Telegram Chess Bot
//!
//! This bot uses [`teloxide`] to access telegram's bot api
//! For chess move generation/validation it uses [`shakmaty`]
//!
//! [`teloxide`]: https://docs.rs/teloxide/0.12.2/teloxide/
//! [`shakmaty`]: https://docs.rs/shakmaty/0.23.1/shakmaty/

use std::sync::{Arc, RwLock};

use shakmaty::Position;
use teloxide::{prelude::*, types::InputFile};

pub mod logging;
pub mod render;
pub mod resources;
pub mod util;

#[tokio::main]
pub async fn main() -> anyhow::Result<()> {
    logging::setup_logging()?;
    dotenv::dotenv().ok();
    log::info!("Starting the bot...");
    let game = Arc::new(RwLock::new(shakmaty::Chess::default()));
    let bot = Bot::from_env();

    let handler = Update::filter_message().endpoint(
        |bot: Bot, game: Arc<RwLock<shakmaty::Chess>>, msg: Message| async move {
            if msg.text().is_none() {
                return respond(());
            }
            log::debug!("Message {}", msg.text().unwrap());
            let Ok(san) = util::text2san(msg.text().unwrap()) else {
                return respond(());
            };
            let Ok(mv) = san.to_move(&game.read().unwrap().clone()) else {
                bot.send_message(msg.chat.id, "This move is not possible").await?;
                return respond(());
            };
            game.write().unwrap().play_unchecked(&mv);
            render::render_to_img(&game.read().unwrap());
            let inputfile = InputFile::file("renders/latest.png");
            bot.send_photo(msg.chat.id, inputfile).send().await?;
            let outcome = game.read().unwrap().outcome();
            if outcome.is_some() {
                let outcome = outcome.unwrap();
                let mut end_message = "The game is drawn";
                if outcome.winner().is_some() {
                    if outcome.winner().unwrap().is_white() {
                        end_message = "White wins";
                    } else {
                        end_message = "Black wins";
                    }
                }
                bot.send_message(msg.chat.id, end_message).send().await?;
                *game.write().unwrap() = shakmaty::Chess::new();
            }
            respond(())
        },
    );

    Dispatcher::builder(bot, handler)
        // Pass the shared state to the handler as a dependency.
        .dependencies(dptree::deps![game])
        .enable_ctrlc_handler()
        .build()
        .dispatch()
        .await;
    Ok(())
}
